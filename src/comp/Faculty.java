package comp;

public class Faculty {

	
	int fid;
	String fname;
	Address address;
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	
	@Override
	public String toString() {
		// TODO Au"to-generated method stub
		return fid+" "+fname+" "+address.laddress+" "+address.paddress;
	}
}
