package comp;

public class Student {

	private String name;
	private int id;
	
	Address address;
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		// TODO Au"to-generated method stub
		return id+" "+name+" "+address.laddress+" "+address.paddress;
	}
}
