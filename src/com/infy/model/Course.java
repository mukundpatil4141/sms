package com.infy.model;

public class Course {
	
	 int cId=0;
	String cName;
	public static int id=0;
	public int getcId() {
		return cId;
	}
	public void setcId(int cId) {
		
		this.cId = cId;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	
	@Override
	public String toString() {
	
		return cId+"  "+cName;
	}

}
