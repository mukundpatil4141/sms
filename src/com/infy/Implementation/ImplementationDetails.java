package com.infy.Implementation;

import java.awt.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.infy.model.Course;
import com.infy.model.Faculty;
import com.infy.model.Student;

public class ImplementationDetails {

	Scanner scanner=new Scanner(System.in);
	ArrayList<Course> allcourse=new ArrayList<Course>();
	ArrayList<Faculty> allfaculty=new ArrayList<Faculty>();
	ArrayList<Student> allstudent=new ArrayList<Student>();
	public void addCourse(){
		System.out.println("how many course you want");
		int number=	scanner.nextInt();

		for(int i=0;i<number;i++){
			Course	course	=new Course();
			/*System.out.println("please enter course id ");
			int cid=	scanner.nextInt();*/
			Course.id++;
			course.setcId(Course.id);
			System.out.println("please enter course name ");
			String cname=	scanner.next();
			course.setcName(cname);
			allcourse.add(course);	
		}

	}

	public void displayCourse(){

		for (Object course : allcourse) {
			System.out.println(course);
		}

	}

	public void addFaculty(){
		System.out.println("how many Faculty you want");
		int number=	scanner.nextInt();

		for(int i=0;i<number;i++){
			Faculty faculty=new Faculty();
			Course	course	=new Course();
			
			System.out.println("please enter faculty id ");
			int fid=	scanner.nextInt();
			faculty.setfId(fid);
			System.out.println("please enter Faculty name ");
			String fname=scanner.next();
			faculty.setfName(fname);
			
			displayCourse();
			System.out.println("please enter Course name for this faculty ");
			String selecteCourse=	scanner.next();
			course.setcName(selecteCourse);
			faculty.setCourse(course);
			allfaculty.add(faculty);	
		}
	}
	public void displayFaculty(){

		for (Faculty faculty : allfaculty) {
			System.out.println(faculty.getfId()+"  "+faculty.getfName()+"  "+faculty.getCourse().getcName());
		}

	}

	public void facultySearch(){
		System.out.println("Please enter facluty name for search...");
		String fanme=	scanner.next();
		for (Faculty faculty : allfaculty) {
			if(fanme.equals(faculty.getfName())){
				System.out.println("SEARCHED FACULTY...."+faculty);
			}
			
		}
		
	}
	
	public void addStudent(){
		System.out.println("how many Student you want");
		int number=	scanner.nextInt();

		for(int i=0;i<number;i++){
			Faculty faculty=new Faculty();
			Course	course	=new Course();
			Student student=new  Student();
			
			System.out.println("please enter Student id ");
			int sid=	scanner.nextInt();
			student.setsId(sid);
			System.out.println("please enter Student name ");
			String sname=	scanner.next();
			student.setsName(sname);
			
			displayFaculty();
			System.out.println("please enter Faculty name for this Student ");
			String selectefaculty=	scanner.next();
			faculty.setfName(selectefaculty);
			for (Faculty faculty2 : allfaculty) {
				if(selectefaculty.equals(faculty2.getfName())){
				String secectedcourse=	faculty2.getCourse().getcName();
					course.setcName(secectedcourse);
					faculty.setCourse(course);
				}
			}
			student.setFaculty(faculty);
			allstudent.add(student);	
		}
	}
	
	public void displayStudent(){

		for (Student student : allstudent) {
			System.out.println(student.getsId()+" "+student.getsName()+" "+student.getFaculty().getfName()+"  "
		+student.getFaculty().getCourse().getcName());
		}

	}
	
}
